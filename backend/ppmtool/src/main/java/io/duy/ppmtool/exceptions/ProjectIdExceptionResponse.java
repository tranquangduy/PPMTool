package io.duy.ppmtool.exceptions;

public class ProjectIdExceptionResponse {

	private String projectIdentifer;

	public ProjectIdExceptionResponse(String projectIdentifier) {
		this.projectIdentifer = projectIdentifier;
	}

	public String getProjectIdentifer() {
		return projectIdentifer;
	}

	public void setProjectIdentifer(String projectIdentifer) {
		this.projectIdentifer = projectIdentifer;
	}

}
